#include "wxGravityFrame.hpp"
#include <wx/dcgraph.h>
#include "Graphics/wxContextRenderer.h"
#include "Graphics/wxGraphicsUtils.h"

namespace JustSnapIt {

wxGravityFrame::wxGravityFrame()
: wxFrame(nullptr, wxID_ANY, wxT("GravityApp"), wxDefaultPosition, wxSize(1200, 600))
, _timer(this, wxID_ANY)
{
    _space.add({30.0, {50.0, 50.0}, {0.50, 0.50}, {255, 0, 0}});
    _space.add({200.0, {250.0, 150.0}, {0.50, -0.50}, {0, 255, 0}});
    _space.add({400.0, {550.0, 250.0}, {0.50, 0.50}, {0, 0, 255}});
    _space.add({10.0, {50.0, 250.0}, {0.50, -0.50}, {0, 255, 255}});

    _timer.Start(10);

    Bind(wxEVT_PAINT, &wxGravityFrame::onPaint, this);
    Bind(wxEVT_ERASE_BACKGROUND, &wxGravityFrame::onEraseBackground, this);
    Bind(wxEVT_TIMER, &wxGravityFrame::onTimer, this);
}

void wxGravityFrame::onPaint(wxPaintEvent& event)
{
    wxPaintDC dc(this);

    draw(dc);
}

void wxGravityFrame::onEraseBackground(wxEraseEvent& event)
{
}

void wxGravityFrame::onTimer(wxTimerEvent& event)
{
    _space.tick();
    Refresh();
}

void wxGravityFrame::draw(wxDC &dc)
{
    wxContextRenderer renderer(dc);
    _space.draw(renderer);
}

}
