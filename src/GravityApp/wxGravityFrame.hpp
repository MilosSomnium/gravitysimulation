#pragma once
#include <wx/wx.h>
#include "Gravity/Space.h"

namespace Graphics {
class Context;
}

namespace JustSnapIt {
class wxGravityFrame: public wxFrame
{
public:
    wxGravityFrame();

    void onPaint(wxPaintEvent& event);
    void onEraseBackground(wxEraseEvent & event);
    void onTimer(wxTimerEvent& event);

    void draw(wxDC &dc);

private:
    Gravity::Space _space;
    wxTimer _timer;
};

}