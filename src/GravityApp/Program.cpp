#include "Program.hpp"
#include "Utils/Modules/ApplicationDefs.hpp"
#include "Utils/Modules/ApplicationExecutor.h"
#include "Utils/Modules/EntryContext.h"
#include "wxGravityFrame.hpp"
#if defined(WIN32) 
#include <wtypes.h>
#endif
#include <memory>
#include <iostream>

namespace {
class EntryVisitor : public boost::static_visitor<void>
{
public:
    void operator()(const aml::args_t& args) const
    {
        auto argc = args.argc();
        wxEntry(argc, args.argv()) ;
    }
    
    void operator()(const aml::wargs_t& args) const
    {
        auto argc = args.argc();
        wxEntry(argc, args.argv());
    }

    void operator()(const aml::args_inst_t& args) const
    {
#if defined(WIN32)
        wxEntry(static_cast<HINSTANCE>(args.instance()), static_cast<HINSTANCE>(args.prevInstance()), args.cmdLine(), args.cmdShow());
#endif
    }
};

class MyApp: public wxApp
{
public:
    bool OnInit() override;
};

bool MyApp::OnInit()
{
    ::wxInitAllImageHandlers();
    
    auto* frame = new JustSnapIt::wxGravityFrame();
    frame->Show();
    return true;
}

}

int EntryPoint(aml::EntryContext args);

MAIN(EntryPoint)

int EntryPoint(const aml::EntryContext args)
{
    wxApp::SetInstance(new MyApp());
    boost::apply_visitor(EntryVisitor(), args);
}