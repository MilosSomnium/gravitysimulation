#pragma once
#include <stack>
#include <vector>
#include "Utils/Graphics/Defs.h"
#include "Utils/Graphics/Context.h"
#include "wx/wx.h"

class wxDC;
class wxGraphicsContext;
namespace JustSnapIt {

class wxContextRenderer : public ::Graphics::Context
{
private:
    struct GraphicsState
    {
        wxRegion region;
        wxPen pen;
        wxBrush brush;
    };

public:
    explicit wxContextRenderer(wxDC& context);

    void setPenColor(const Graphics::Color& color) override;
    void setBrushColor(const Graphics::Color& color) override;
    void setLineWidth(Graphics::Coord width) override;
    void setLineStyle(Graphics::LineStyle width) override;

    void drawText(ptl::string_view text, const Graphics::Point& pos) override;
    void drawRectangle(const Graphics::Rect& rect) override;
    void drawRoundedRectangle(const Graphics::Rect& rect, Graphics::Coord radius) override;
    void drawLine(const Graphics::Point& first, const Graphics::Point& last) override;
    void drawCircle(const Graphics::Point& pos, Graphics::Coord radius) override;

    void clip(const Graphics::Region& region) override;
    void clip(const Graphics::Rect& rect, Graphics::ClipMode mode) override;
    void resetClip() override;
    void saveState() override;
    void restoreState() override;

    Graphics::Size getTextSize(ptl::string_view text) const override;

private:
    wxDC& _context;
    std::stack<std::unique_ptr<GraphicsState>, std::vector<std::unique_ptr<GraphicsState>>> _state;
};

}