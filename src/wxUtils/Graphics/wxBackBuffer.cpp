#include "wxBackBuffer.h"
#include <wx/dcgraph.h>

namespace JustSnapIt {
wxBackBuffer::wxBackBuffer(const wxSize& size)
: m_buffer(size)
, m_memoryDC(m_buffer)
, m_gcDC(std::make_unique<wxGCDC>(m_memoryDC))
{
#ifdef BUILD_WINDOWS
    m_buffer.UseAlpha();
#endif
}

bool wxBackBuffer::IsOk() const
{
    return m_buffer.IsOk() && m_gcDC;
}

wxSize wxBackBuffer::GetSize() const
{
    return m_buffer.IsOk() ? m_buffer.GetSize() : wxDefaultSize;
}

wxDC& wxBackBuffer::GetDC() const
{
    return *m_gcDC;
}

void wxBackBuffer::Clear(const wxColor& background)
{
    m_memoryDC.SetBackground(background);
    m_memoryDC.Clear();
}

bool wxBackBuffer::CopyTo(wxDC& dc)
{
    return CopyTo(dc, GetSize());
}

bool wxBackBuffer::CopyTo(wxDC& dc, const wxRect& rect)
{
    if (rect.IsEmpty() || !m_buffer.IsOk())
    {
        return false;
    }
    if (!dc.Blit(rect.GetPosition(), rect.GetSize(), m_gcDC.get(), rect.GetPosition()))
    {
        auto result = false;
        m_memoryDC.SelectObject(wxNullBitmap);        
        wxRect bitmapRect(GetSize());
        if (bitmapRect == rect)
        {
            dc.DrawBitmap(m_buffer, rect.GetPosition());
            result = true;
        }
        else
        {
            bitmapRect.Intersect(rect);
            if (!bitmapRect.IsEmpty())
            {
                auto bitmap = m_buffer.GetSubBitmap(bitmapRect);
                if (bitmap.IsOk())
                {
                    dc.DrawBitmap(bitmap, bitmapRect.GetPosition());
                    result = true;
                }
            }
        }
        m_memoryDC.SelectObject(m_buffer);
        return result;
    }
    return false;
}

void wxBackBuffer::Resize(const wxSize& size)
{
    if (!m_buffer.IsOk() || size != m_buffer.GetSize())
    {
        m_memoryDC.SelectObject(wxNullBitmap);
        
#ifdef BUILD_WINDOWS
        m_buffer.FreeResource();
#endif
        m_buffer.Create(size, 32);
        m_memoryDC.SelectObject(m_buffer);

        m_gcDC = std::make_unique<wxGCDC>(m_memoryDC);
#ifdef BUILD_WINDOWS
        m_buffer.UseAlpha();
#endif
    }
}

bool wxBackBuffer::SaveToFile(const wxString& filename)
{
    return SaveToFile(filename, GetSize());
}

bool wxBackBuffer::SaveToFile(const wxString& filename, const wxRect& rect)
{
    bool result{false};
    wxRect bitmapRect(GetSize());
    if (bitmapRect == rect)
    {
        m_memoryDC.SelectObject(wxNullBitmap);
        result = m_buffer.SaveFile(filename + wxT(".png"), wxBitmapType::wxBITMAP_TYPE_PNG);
        m_memoryDC.SelectObject(m_buffer);
    }
    else
    {
        bitmapRect.Intersect(rect);
        m_memoryDC.SelectObject(wxNullBitmap);
        auto bitmap = m_buffer.GetSubBitmap(bitmapRect);
        m_memoryDC.SelectObject(m_buffer);
        result = bitmap.SaveFile(filename + wxT(".png"), wxBitmapType::wxBITMAP_TYPE_PNG);
    }

    return result;
}

void wxBackBuffer::ProcessBuffer(TBufferProcessor processor)
{
    m_memoryDC.SelectObject(wxNullBitmap);
    processor(m_buffer);
    m_memoryDC.SelectObject(m_buffer);
}

}
