#include "wxGraphicsUtils.h"
#include "Utils/Graphics/Point.h"
#include "Utils/Graphics/Size.h"
#include "Utils/Graphics/Rect.h"
#include "Utils/Graphics/Color.h"

namespace JustSnapIt 
{
namespace
{
wxCoord towxCoord(const Graphics::Coord coord)
{
    return wxRound(coord);
}
}
wxPoint fromPoint(const Graphics::Point& point)
{
    return {towxCoord(point.x()), towxCoord(point.y())};
}

wxSize fromSize(const Graphics::Size& size)
{
    return {towxCoord(size.width()), towxCoord(size.height())};
}

wxRect fromRect(const Graphics::Rect& rect)
{
    return {fromPoint(rect.getTopLeft()), fromSize(rect.getSize())};
}

wxColour fromColour(const Graphics::Color& color)
{
    return {color.r(), color.g(), color.b(), color.a()};
}

Graphics::Point toPoint(const wxPoint& point)
{
    return {Graphics::Coord(point.x), Graphics::Coord(point.y)};
}

Graphics::Size toSize(const wxSize& size)
{
    return {Graphics::Coord(size.GetWidth()), Graphics::Coord(size.GetHeight())};
}

Graphics::Rect toRect(const wxRect& rect)
{
    return {toPoint(rect.GetTopLeft()), toSize(rect.GetSize())};
}

}