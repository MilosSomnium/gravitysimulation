#pragma once
#include <wx/wx.h>

#include "Utils//Graphics/Point.h"
#include "Utils//Graphics/Size.h"
#include "Utils//Graphics/Rect.h"

namespace JustSnapIt {

wxPoint fromPoint(const Graphics::Point& point);
wxSize fromSize(const Graphics::Size& size);
wxRect fromRect(const Graphics::Rect& rect);
wxColour fromColour(const Graphics::Color& rect);

Graphics::Point toPoint(const wxPoint& point);
Graphics::Size toSize(const wxSize& size);
Graphics::Rect toRect(const wxRect& point);

}