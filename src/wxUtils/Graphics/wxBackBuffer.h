#pragma once
#include <functional>
#include <unordered_map>
#include <memory>
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

class wxGCDC;

namespace JustSnapIt{
class wxBackBuffer
{
public:
    using TBufferProcessor = std::function<void(wxBitmap&)>;

public:
    wxBackBuffer() = default;
    explicit wxBackBuffer(const wxSize& size);
    ~wxBackBuffer() = default;

    bool IsOk() const;
    wxSize GetSize() const;
    wxDC& GetDC() const;

    void Clear(const wxColor& background);

    bool CopyTo(wxDC& dc);
    bool CopyTo(wxDC& dc, const wxRect& rect);

    void Resize(const wxSize& size);

    bool SaveToFile(const wxString& filename);
    bool SaveToFile(const wxString& filename, const wxRect& rect);

    void ProcessBuffer(TBufferProcessor processor);

private:
    wxBitmap m_buffer;
    wxMemoryDC m_memoryDC;
    std::unique_ptr<wxGCDC> m_gcDC;
};
}