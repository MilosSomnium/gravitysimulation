#include "wxContextRenderer.h"
#include <wx/wx.h>
#include <wx/dc.h>
#include "Utils/Graphics/Defs.h"
#include "Utils/Graphics/Point.h"
#include "Utils/Graphics/Size.h"
#include "Utils/Graphics/Rect.h"
#include "Utils/Graphics/Color.h"
#include "wxGraphicsUtils.h"

using namespace Graphics;

namespace JustSnapIt {

wxContextRenderer::wxContextRenderer(wxDC& context)
: _context(context)
{
    _state.push(std::make_unique<GraphicsState>());
    _state.top()->pen = _context.GetPen();
    _state.top()->brush = _context.GetBrush();
}

void wxContextRenderer::setPenColor(const Graphics::Color& color)
{
    _state.top()->pen.SetColour(fromColour(color));
    _context.SetPen(_state.top()->pen);
}

void wxContextRenderer::setBrushColor(const Graphics::Color& color)
{
    _state.top()->brush.SetColour(fromColour(color));
    _context.SetBrush(_state.top()->brush);
}

void wxContextRenderer::setLineWidth(Graphics::Coord width)
{
    _state.top()->pen.SetWidth(wxRound(width));
    _context.SetPen(_state.top()->pen);
}

void wxContextRenderer::setLineStyle(Graphics::LineStyle style)
{
    switch(style)
    {
        case Graphics::LineStyle::Invalid:
            _state.top()->pen.SetStyle(wxPenStyle::wxPENSTYLE_INVALID);
            break;
        case Graphics::LineStyle::Solid:
            _state.top()->pen.SetStyle(wxPenStyle::wxPENSTYLE_SOLID);
            break;
        case Graphics::LineStyle::Dot:
            _state.top()->pen.SetStyle(wxPenStyle::wxPENSTYLE_DOT);
            break;
        case Graphics::LineStyle::LongDash:
            _state.top()->pen.SetStyle(wxPenStyle::wxPENSTYLE_LONG_DASH);
            break;
        case Graphics::LineStyle::ShortDash:
            _state.top()->pen.SetStyle(wxPenStyle::wxPENSTYLE_SHORT_DASH);
            break;
        case Graphics::LineStyle::DotDash:
            _state.top()->pen.SetStyle(wxPenStyle::wxPENSTYLE_DOT_DASH);
            break;
    }
    _context.SetPen(_state.top()->pen);
}

void wxContextRenderer::drawText(ptl::string_view text, const Graphics::Point& pos)
{
    _context.DrawText(wxString(text.data()), wxRound(pos.x()), wxRound(pos.y()));
}

void wxContextRenderer::drawRectangle(const Graphics::Rect& rect)
{
    _context.DrawRectangle(fromRect(rect));
}

void wxContextRenderer::drawLine(const Point& first, const Point& last)
{
    _context.DrawLine(wxRound(first.x()), wxRound(first.y()), wxRound(last.x()), wxRound(last.y()));
}

void wxContextRenderer::drawRoundedRectangle(const Graphics::Rect& rect, const Graphics::Coord radius)
{
    _context.DrawRoundedRectangle(fromRect(rect), static_cast<double>(radius));
}

void wxContextRenderer::drawCircle(const Graphics::Point& pos, const Graphics::Coord radius)
{
    _context.DrawCircle(wxRound(pos.x()), wxRound(pos.y()), wxRound(radius));
}

void wxContextRenderer::clip(const Graphics::Region& region)
{
    //_state.top()->region.Intersect(FromRect(rect));
    _context.SetDeviceClippingRegion(_state.top()->region);
}

void wxContextRenderer::clip(const Graphics::Rect& rect, Graphics::ClipMode mode)
{
    if(_state.top()->region.IsOk())
    {
        switch(mode)
        {
            case Graphics::ClipMode::And:
                _state.top()->region.Intersect(fromRect(rect));
                break;
            case Graphics::ClipMode::Xor:
                _state.top()->region.Subtract(fromRect(rect));
                break;
        }
    }
    else
    {
        _state.top()->region = wxRegion(fromRect(rect));
    }

    _context.SetDeviceClippingRegion(_state.top()->region);
}

void wxContextRenderer::resetClip()
{
    _state.top()->region.Clear();
    _context.DestroyClippingRegion();
}

void wxContextRenderer::saveState()
{
    const auto& top = *_state.top();
    _state.push(std::make_unique<GraphicsState>(top));
}

void wxContextRenderer::restoreState()
{
    _state.pop();
    _context.SetPen(_state.top()->pen);
    _context.SetBrush(_state.top()->brush);
    _context.DestroyClippingRegion();
    if(!_state.top()->region.IsEmpty())
    {
        _context.SetDeviceClippingRegion(_state.top()->region);
    }
}


Graphics::Size wxContextRenderer::getTextSize(ptl::string_view text) const
{
    return toSize(_context.GetTextExtent(wxString(text.data())));
}

}