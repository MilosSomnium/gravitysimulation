project(${FMT_LIB_NAME})

set(FMT_FOUND 1)

set(FMT_HEADERS fmt/format.h)
if(WIN32)
	set(FMT_LIBS fmt.lib)
	set(FMT_LIBS_DEBINFO fmt.pdb)
else()
    set(FMT_LIBS libfmt.a)
endif()


set(FMT_FILES)
foreach(HEADER IN LISTS FMT_HEADERS)
    list(APPEND FMT_FILES ${FMT_PREFIX}/include/${HEADER})
endforeach()
foreach(LIB IN LISTS FMT_LIBS)
    list(APPEND FMT_FILES ${FMT_PREFIX}/lib/${LIB})
endforeach()

if(FMT_FILES)
    foreach(FILE IN LISTS FMT_FILES)
        if(NOT EXISTS "${FILE}")
            unset(FMT_FOUND)
        endif()
    endforeach()
else()
    message(FATAL_ERROR "Empty FMT files list")
endif()

if(NOT FMT_FOUND)
    download_file(${DOWNLOAD_URL} ${FMT_FILENAME} ${DOWNLOAD_DIR} ${FMT_MD5_HASH})
    extract_file(${DOWNLOAD_DIR}/${FMT_FILENAME} ${FMT_BUILD_DIR})

    file(MAKE_DIRECTORY "${FMT_STAMP}")
    message(STATUS "Configuring ${FMT_LIB_NAME} ${FMT_VERSION} ...")
    set(FMT_CONFIGURE_OPT
    -G ${CMAKE_GENERATOR}
    -DFMT_DOC=false
    -DCMAKE_INSTALL_PREFIX=${FMT_PREFIX}
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} 
    )
    
    set(FMT_CONFIGURE_COMAND ${CMAKE_COMMAND} ${FMT_CONFIGURE_OPT} ${FMT_BUILD_DIR}/)
    execute_process(
        COMMAND ${FMT_CONFIGURE_COMAND}
        WORKING_DIRECTORY ${FMT_BUILD_DIR}
        RESULT_VARIABLE FMT_CONFIGURE_RESULT
        OUTPUT_VARIABLE FMT_CONFIGURE_OUTPUT
        OUTPUT_FILE "${FMT_STAMP}/fmt-configure-out.log"
        ERROR_FILE "${FMT_STAMP}/fmt-configure-err.log"
    )
    if(FMT_CONFIGURE_RESULT)
    message(STATUS "${FMT_CONFIGURE_OUTPUT}")
        set(msg "Configure failed: ${FMT_CONFIGURE_RESULT}\n")
        foreach(arg IN LISTS FMT_CONFIGURE_COMAND)
        set(msg "${msg} '${arg}'")
        endforeach()
        set(msg "${msg}\nSee also\n  ${FMT_STAMP}/fmt-configure-*.log")
        message(FATAL_ERROR "${msg}")
    else()
        set(msg "fmt configure command succeeded.  See also ${FMT_STAMP}/fmt-configure-*.log")
        message(STATUS "${msg}")
    endif()

    set(CORES_BUILD_ARG)
    if(NOT WIN32)
        set(CORES_BUILD_ARG -- -j${CPU_COUNT})
    endif()
    
    set(FMT_BUILD_COMAND ${CMAKE_COMMAND} --build . --target install ${CORES_BUILD_ARG})
    execute_process(
        COMMAND ${FMT_BUILD_COMAND}
        WORKING_DIRECTORY ${FMT_BUILD_DIR}
        RESULT_VARIABLE FMT_BUILD_RESULT
        OUTPUT_VARIABLE FMT_BUILD_OUTPUT
        OUTPUT_FILE "${FMT_STAMP}/fmt-build-out.log"
        ERROR_FILE "${FMT_STAMP}/fmt-build-err.log"
    )
    if(FMT_BUILD_RESULT)
        message(STATUS "${FMT_BUILD_OUTPUT}")
        set(msg "Build failed: ${FMT_CONFIGURE_RESULT}\n")
        foreach(arg IN LISTS FMT_BUILD_RESULT)
        set(msg "${msg} '${arg}'")
        endforeach()
        set(msg "${msg}\nSee also\n  ${FMT_STAMP}/fmt-build-*.log")
        message(FATAL_ERROR "${msg}")
    else()
        set(msg "fmt build command succeeded.  See also ${FMT_STAMP}/fmt-build-*.log")
        message(STATUS "${msg}")
    endif()

    # ExternalProject_Add(
    #     ${FMT_LIB_NAME}
    #     PREFIX              ${FMT_PREFIX}
    #     TMP_DIR             ${3RD_PARTY_TEMP_DIR}
    #     STAMP_DIR           ${FMT_PREFIX}/stamp
    #     # Download
    #     #DOWNLOAD_DIR        ${DOWNLOAD_DIR}
    #     #DOWNLOAD_NAME       ${FMT_FILENAME}
    #     #URL                 ${DOWNLOAD_URL}/${FMT_FILENAME}
    #     #URL_MD5             ${FMT_MD5}
    #     # Patch
    #     #PATCH_COMMAND       ${PATCH_COMMAND} ${PROJECT_SOURCE_DIR}/patch.diff
    #     # Configure
    #     SOURCE_DIR          ${FMT_BUILD_DIR}
    #     CMAKE_COMMAND       ${CMAKE_COMMAND}
    #     CMAKE_ARGS          ${CMAKE_OPTS}

    #     # Build
    #     BUILD_IN_SOURCE     1
    #     #BUILD_BYPRODUCTS    ${CUR_COMPONENT_ARTIFACTS}
    #     # Install
    #     # Need to do it manually, since the "install" target is not generated for this lib.
    #     INSTALL_COMMAND     ${CMAKE_COMMAND} -E copy ${FMT_BUILD_DIR}/fmt/format.h ${FMT_PREFIX}/include/fmt/format.h
    #                         COMMAND ${CMAKE_COMMAND} -E copy ${FMT_BUILD_DIR}/fmt/libfmt.a ${FMT_PREFIX}/lib/libfmt.a

                                                    
    #     # Logging
    #     LOG_DOWNLOAD        1
    #     LOG_CONFIGURE       1
    #     LOG_BUILD           1
    #     LOG_INSTALL         1
    # )
endif()

foreach(LIB IN LISTS FMT_LIBS)
    list(APPEND FMT_LIBRARIES ${FMT_PREFIX}/lib/${LIB})
endforeach()

set(FMT_INCLUDE_DIRS ${FMT_INCLUDEDIR} PARENT_SCOPE)
set(FMT_LIBRARIES ${FMT_LIBRARIES} PARENT_SCOPE)