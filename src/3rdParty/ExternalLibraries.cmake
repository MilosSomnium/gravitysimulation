
macro(download_file URL FILE DESTINATION MD5_HASH)
    set(SOURCE_FILE ${URL}/${FILE})
    set(DEST_FILE ${DESTINATION}/${FILE})
    
    if(EXISTS "${SOURCE_FILE}")
        message(STATUS "coping to ${DEST_FILE} ...")
        #file(COPY ${SOURCE_FILE} ${DEST_FILE})
        execute_process(COMMAND ${CMAKE_COMMAND} -E copy ${SOURCE_FILE} ${DEST_FILE})
    else()
        message(STATUS "downloading to ${DEST_FILE} ...")
        file(DOWNLOAD ${SOURCE_FILE} ${DEST_FILE})
    endif()

    file("MD5" "${DEST_FILE}" LOCAL_HASH)
    if(NOT "${MD5_HASH}" STREQUAL "${LOCAL_HASH}")
        message(FATAL_ERROR "error: MD5 hash of
                 ${SOURCE_FILE}
                 does not match expected value
                 expected: '${MD5_HASH}'
                 actual: '${LOCAL_HASH}'
        ")
    endif() 
endmacro()

macro(extract_file FILE DESTINATION)
    file(MAKE_DIRECTORY "${DESTINATION}")
    message(STATUS "extracting ${FILE} to ${DESTINATION} ...")
    execute_process(COMMAND ${CMAKE_COMMAND} -E tar xfz ${FILE}
        WORKING_DIRECTORY ${DESTINATION}
        #OUTPUT_FILE "/Users/sergeim/3rdParty/Install/boost/1.64.0/stamp/boost-download-out.log"
        #ERROR_FILE "/Users/sergeim/3rdParty/Install/boost/1.64.0/stamp/boost-download-err.log"
        ERROR_VARIABLE error
        RESULT_VARIABLE rv)
    if(NOT rv EQUAL 0)
        message(STATUS "extracting... [error clean up]")
        file(REMOVE_RECURSE "${DESTINATION}")
        message(FATAL_ERROR "error: extract of '${FILE}' failed:
                ${ERROR_VARIABLE}
        ")
    endif()
endmacro()

macro(add_external_library VAR_NAME LIB_NAME VERSION MD5_HASH)

    set(${VAR_NAME}_LIB_NAME ${LIB_NAME})
    set(${VAR_NAME}_VERSION ${VERSION})
    set(${VAR_NAME}_FILENAME ${LIB_NAME}-${VERSION}.zip)
    set(${VAR_NAME}_MD5_HASH ${MD5_HASH})
    set(${VAR_NAME}_PREFIX ${3RD_PARTY_INSTALL_DIR}/${LIB_NAME}/${VERSION}/${BUILD_TOOL}/${BUILD_PLATFORM}/${BUILD_TYPE})
    set(${VAR_NAME}_STAMP ${${VAR_NAME}_PREFIX}/stamp)
    set(${VAR_NAME}_BUILD_DIR ${3RD_PARTY_BUILD_DIR}/${LIB_NAME}/${VERSION})
    set(${VAR_NAME}_INCLUDEDIR ${${VAR_NAME}_PREFIX}/include)
    set(${VAR_NAME}_LIBRARYDIR ${${VAR_NAME}_PREFIX}/lib)
    

    message(STATUS "Processing ${LIB_NAME} ${${VAR_NAME}_VERSION}")

    add_subdirectory(3rdParty/${LIB_NAME})
endmacro()