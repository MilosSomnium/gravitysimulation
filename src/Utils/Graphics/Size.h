#pragma once
#include "Defs.h"
#include <iostream>

namespace Graphics
{
template<typename CoordinateType>
class Size2D 
{
public:
    Size2D() = default;

    Size2D(CoordinateType const& w, CoordinateType const& h)
        : _width(w)
        , _height(h)
    {}

    friend bool operator==(const Size2D& left, const Size2D& right)
    {
        return (left._width == right._width) && (left._height == right._height);
    }

    friend std::ostream& operator<<(std::ostream& output, const Size2D& size)
    {
        output << "( " << size._width << "; " << size._height << " )";
        return output;            
    }

    CoordinateType width() const
    {
        return _width;
    }

    CoordinateType height() const
    {
        return _height;
    }

    void setWidth(CoordinateType v)
    {
        _width = v;
    }

    void setHeight(CoordinateType v)
    {
        _height = v;
    }

    bool isEmpty() const
    {
        return _width == 0 || _height == 0;
    }

private:
    CoordinateType _width;
    CoordinateType _height;
};

using Size = Size2D<Coord>;
}