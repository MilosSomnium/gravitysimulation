#pragma once

namespace Graphics
{
using Coord = float;

class Color;
class Region;
class Path;
class Font;
}