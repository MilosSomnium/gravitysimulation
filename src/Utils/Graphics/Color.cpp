#include "Color.h"

namespace Graphics{

Color::Color()
: _r(0), _g(0), _b(0), _a(0)
{}

Color::Color(value_type r, value_type g, value_type b, value_type a)
: _r(r), _g(g), _b(b), _a(a)
{}

Color::value_type Color::r() const
{
    return _r;
}

Color::value_type Color::g() const
{
    return _g;
}

Color::value_type Color::b() const
{
    return _b;
}

Color::value_type Color::a() const
{
    return _a;
}
}