#pragma once
#include "Defs.h"
#include <iostream>
#include <cmath>

namespace Graphics
{
class Point
{
public:
    using CoordinateType = Coord;

public:
    Point();

    Point(CoordinateType x, CoordinateType y);

    void setX(CoordinateType v);
    void setY(CoordinateType v);

    CoordinateType x() const;
    CoordinateType y() const;

    friend Point operator*(const Point& left, const CoordinateType right)
    {
        return {left._x * right, left._y * right};
    }

    friend Point operator/(const Point& left, const CoordinateType right)
    {
        return {left._x / right, left._y / right};
    }
    friend Point operator-(const Point& left, const Point& right)
    {
        return {left._x - right._x, left._y - right._y};
    }

    Point& operator+=(const Point& that)
    {
        _x += that._x;
        _y += that._y;
        return *this;
    }

    friend Point operator+(const Point& left, const Point& right)
    {
        Point result(left);
        result+= right;
        return result;
    }
    friend bool operator==(const Point& left, const Point& right)
    {
        return (left._x == right._x) && (left._y == right._y);
    }

    friend bool operator!=(const Point& left, const Point& right)
    {
        return (left._x != right._x) || (left._y != right._y);
    }

    friend std::ostream& operator<<(std::ostream& output, const Point& point)
    {
        output << "( " << point._x << "; " << point._y << " )";
        return output;            
    }

    friend Point::CoordinateType length(const Point& vector)
    {
        return std::sqrt(vector.x()*vector.x() + vector.y()*vector.y());
    }

    friend Point normalize(const Point& vector)
    {
        const auto invS = 1 / length(vector);
        Point result;
        result.setX(vector.x() * invS);
        result.setY(vector.y() * invS);
        return result;
    }

private:
    CoordinateType _x;
    CoordinateType _y;
};
}