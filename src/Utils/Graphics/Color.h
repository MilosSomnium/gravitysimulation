#pragma once
#include <cstdint>
#include <limits>

namespace Graphics
{
class Color
{
public:
    using value_type = std::uint8_t;

    static const value_type AlphaTransparent = 0;
    static const value_type AlphaOpaque = 0xFF;

public:
    Color();
    Color(value_type r, value_type g, value_type b, value_type a = AlphaOpaque);

    value_type r() const;
    value_type g() const;
    value_type b() const;
    value_type a() const;

private:
    value_type _r;
    value_type _g;
    value_type _b;
    value_type _a;
};

}