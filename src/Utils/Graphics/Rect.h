#pragma once
#include "Defs.h"
#include "Point.h"
#include "Size.h"

namespace Graphics
{
class Rect
{
public:
    Rect() = default;
    Rect(const Size& size);
    Rect(const Point& pos, const Size& size);

    friend bool operator==(const Rect& left, const Rect& right)
    {
        return (left.getTopLeft() == right.getTopLeft()) && (left.getSize() == right.getSize());
    }

    friend bool operator!=(const Rect& left, const Rect& right)
    {
        return !(left == right);
    }

    friend std::ostream& operator<<(std::ostream& output, const Rect& rect)
    {
        output << "( " << rect._pos << ", " << rect._size << " )";
        return output;            
    }

    Coord x() const;
    Coord y() const;
    Coord width() const;
    Coord height() const;
    const Point& getTopLeft() const;
    const Size& getSize() const;

    bool isEmpty() const;

private:
    Point _pos;
    Size _size;
};

}