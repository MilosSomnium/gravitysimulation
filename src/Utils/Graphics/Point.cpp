//
// Created by SergeiM on 23.05.18.
//
#include "Point.h"

namespace Graphics
{

Point::Point()
        : _x(0)
        , _y(0)
{}

Point::Point(CoordinateType x, CoordinateType y)
: _x(x)
, _y(y)
{}


Point::CoordinateType Point::x() const
{
    return _x;
}


Point::CoordinateType Point::y() const
{
    return _y;
}


void Point::setX(CoordinateType v)
{
    _x = v;
}


void Point::setY(CoordinateType v)
{
    _y = v;
}
}