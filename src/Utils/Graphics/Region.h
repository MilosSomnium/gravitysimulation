#pragma once

#include "Rect.h"

namespace Graphics
{

class Region
{
public:
    Region() = default;
    Region(const Region&) = default;
    //Region(const Rect& rect);
};

}