#include "Rect.h"

namespace Graphics
{
Rect::Rect(const Point& pos, const Size& size)
: _pos(pos)
, _size(size)
{}

Rect::Rect(const Size& size)
: _pos(0, 0)
, _size(size)
{}

Coord Rect::x() const
{
    return _pos.x();
}

Coord Rect::y() const
{
    return _pos.x();
}

Coord Rect::width() const
{
    return _size.width();
}

Coord Rect::height() const
{
    return _size.height();
}

const Point& Rect::getTopLeft() const
{
    return _pos;
}

const Size& Rect::getSize() const
{
    return _size;
}

bool Rect::isEmpty() const
{
    return _size.isEmpty();
}

}