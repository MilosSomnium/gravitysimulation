#pragma once
#include "Utils/PTL/StringView.h"
#include "Utils/Graphics/Defs.h"
#include "Utils/Graphics/Point.h"
#include "Utils/Graphics/Rect.h"
#include "Utils/Graphics/Size.h"

namespace Graphics {

enum class LineStyle: std::uint8_t
{
    Invalid,
    Solid,
    Dot,
    LongDash,
    ShortDash,
    DotDash
};

enum class ClipMode: std::uint8_t
{
    And,
    Xor
};

class Context
{
public:
	virtual ~Context() = default;

    virtual void setPenColor(const Color& color) = 0;
    virtual void setBrushColor(const Color& color) = 0;
    virtual void setLineWidth(Coord width) = 0;
    virtual void setLineStyle(LineStyle style) = 0;

    virtual void drawText(ptl::string_view text, const Point& pos) = 0;
    virtual void drawRectangle(const Rect& rect) = 0;
    virtual void drawRoundedRectangle(const Rect& rect, Coord radius) = 0;
    virtual void drawLine(const Point& first, const Point& last) = 0;
    virtual void drawCircle(const Point& pos, Coord radius) = 0;

    virtual void clip(const Region& region) = 0;
    virtual void clip(const Rect& rect, ClipMode mode) = 0;
    virtual void resetClip() = 0;
	virtual void saveState() = 0;
	virtual void restoreState() = 0;

    virtual Size getTextSize(ptl::string_view text) const = 0;
};

class ScopedStateSaver
{
public:
    explicit ScopedStateSaver(Context& context)
        : _context(context)
    {
        _context.saveState();
    }

    ~ScopedStateSaver()
    {
        _context.restoreState();
    }

private:
    Context& _context;
};

}
