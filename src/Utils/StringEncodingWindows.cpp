#include "StringEncoding.hpp"
#include <Windows.h>
#include <memory>
#include <boost/format.hpp>

namespace Utils {
namespace Encoding {

std::string to_utf8(std::uint32_t ch)
{
    std::string result;
    if (ch < 0x80) {        // one octet
        result.resize(1);
        result[0] = static_cast<char const>(ch);
    }
    else if (ch < 0x800) {   // two octets
        result.resize(2);
        result[0] = static_cast<char const>((ch >> 6) | 0xc0);
        result[1] = static_cast<char const>((ch & 0x3f) | 0x80);
    }
    else if (ch < 0x10000) { // three octets
        result.resize(3);
        result[0] = static_cast<char const>((ch >> 12) | 0xe0);
        result[1] = static_cast<char const>(((ch >> 6) & 0x3f) | 0x80);
        result[2] = static_cast<char const>((ch & 0x3f) | 0x80);
    }
    else {                   // four octets
        result[0] = static_cast<char const>((ch >> 18) | 0xf0);
        result[1] = static_cast<char const>(((ch >> 12) & 0x3f) | 0x80);
        result[2] = static_cast<char const>(((ch >> 6) & 0x3f) | 0x80);
        result[3] = static_cast<char const>((ch & 0x3f) | 0x80);
    }
    return result;
}

std::string to_utf8(ptl::wstring_view source)
{
    std::string result;

    int size = ::WideCharToMultiByte(
        CP_UTF8,
        0,
        source.data(),
        static_cast<int>(source.size()),
        nullptr,
        0,
        nullptr,
        nullptr);
    if (size > 0)
    {
        auto buffer = std::make_unique<char[]>(size + 1);
        int codedSize = ::WideCharToMultiByte(
            CP_UTF8,
            0,
            source.data(),
            static_cast<int>(source.size()),
            buffer.get(),
            size,
            nullptr,
            nullptr);
        if (codedSize == size)
        {
            result = std::string(buffer.get());
        }
        else
        {
            throw std::length_error(boost::str(boost::format("Failed to encode string(encoded %1% of %2%); Error %3%") % codedSize % size % ::GetLastError()));
        }
    }
    else
    {
        throw std::length_error(boost::str(boost::format("Failed to get size of string ; Error %1%") % ::GetLastError()));
    }
    return result;
}

std::wstring to_wstring(ptl::string_view source)
{
    std::wstring result;

    int size = ::MultiByteToWideChar(
        CP_UTF8,
        0,
        source.data(),
        static_cast<int>(source.size()),
        nullptr,
        0);
    if (size > 0)
    {
        auto buffer = std::make_unique<wchar_t[]>(size + 1);
        int codedSize = MultiByteToWideChar(
            CP_UTF8,
            0,
            source.data(),
            static_cast<int>(source.size()),
            buffer.get(),
            size);
        if (codedSize == size)
        {
            result = std::wstring(buffer.get());
        }
        else
        {
            throw std::length_error(boost::str(boost::format("Failed to encode string(encoded %1% of %2%); Error %3%") % codedSize % size % ::GetLastError()));
        }
    }
    else
    {
        throw std::length_error(boost::str(boost::format("Failed to get size of string ; Error %1%") % ::GetLastError()));
    }

    return result;
}

std::size_t utf8_length(ptl::string_view source)
{
    int size = ::MultiByteToWideChar(CP_UTF8, 0, source.data(), static_cast<int>(source.size()), nullptr, 0);
    return size >= 0 ? static_cast<std::size_t>(size) : 0;
}

}
}
