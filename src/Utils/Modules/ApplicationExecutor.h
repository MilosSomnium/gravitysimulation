#pragma once

namespace aml {

template <class Module>
class ApplicationExecutor
{
public:
    explicit ApplicationExecutor(Module& module)
        : m_module(module)
    {}

    ApplicationExecutor(const ApplicationExecutor&) = delete;
    ApplicationExecutor& operator=(const ApplicationExecutor&) = delete;

    ~ApplicationExecutor()
    {
        m_module.Finalize();
    }

    bool Init()
    {
        return m_module.Initialize();
    }

    int Run()
    {
        m_module.Run();
        return m_module.GetExitCode();
    }

private:
    Module& m_module;
};
}