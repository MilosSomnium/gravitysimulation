#include "ApplicationModule.h"

namespace aml {
ApplicationModule::ApplicationModule(const aml::EntryContext& context)
: m_exitCode(EXIT_FAILURE)
, m_context(context)
{}

ApplicationModule::~ApplicationModule()
{

}

void ApplicationModule::Exit(int exitCode)
{
    SetExitCode(exitCode);
    OnStop();
}

ApplicationModule::arguments_t ApplicationModule::GetCommandLine() const
{
    return m_args;
}

const EntryContext& ApplicationModule::GetEntryArgs() const
{
    return m_context;
}

void ApplicationModule::SetExitCode(int exitCode)
{
    m_exitCode = exitCode;
}

int ApplicationModule::GetExitCode() const
{
    return m_exitCode;
}
}