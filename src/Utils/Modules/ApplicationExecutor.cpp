#include "ApplicationExecutor.h"
#include "ApplicationModule.h"
#include "ApplicationInstance.h"

using namespace aml;

ApplicationExecutor::ApplicationExecutor(const ptl::shared_ptr<aml::ApplicationModule>& module)
: m_module(module)
{
}

ApplicationExecutor::~ApplicationExecutor()
{
    m_module->OnExit();
}

bool ApplicationExecutor::Init()
{
    return m_module->OnInit();
}

int ApplicationExecutor::Run()
{
    m_module->OnRun();
    return m_module->GetExitCode();
}

