#pragma once
#include <vector>
#include <memory>
#include "EntryContext.h"

namespace aml {
template <class Module>
class ApplicationExecutor;

class ApplicationModule
{
public:
    using arguments_t = std::vector<const char*>;

public:
    ApplicationModule(const EntryContext& context);
    virtual ~ApplicationModule();

    arguments_t GetCommandLine() const;
    const EntryContext& GetEntryArgs() const;

    void Exit(int exitCode);

    int GetExitCode() const;

private:
    bool Initialize() { return OnInit(); }
    void Run() { return OnRun(); }
    void Finalize() { return OnExit(); }

    template <class Module>
    friend class ApplicationExecutor;

protected:
    virtual bool OnInit() = 0;
    virtual void OnExit() = 0;
    virtual void OnRun() = 0;
    virtual void OnStop() = 0;

    void SetExitCode(int exitCode);

private:
    int m_exitCode;
    const EntryContext& m_context;
    arguments_t m_args;
};

}
