#pragma once
#include "Utils/PTL/Variant.hpp"

namespace aml {

template <class CharT>
class base_args_t
{
public:
    base_args_t(int argc, CharT* argv[])
        : _argc(argc), _argv(argv)
    {}

    int argc() const { return _argc; }
    CharT** argv() const { return _argv; }

private:
    int _argc;
    CharT** _argv;
};

using args_t = base_args_t<char>;
using wargs_t = base_args_t<wchar_t>;
using instance_handle_t = void*;

class args_inst_t
{
public:
    args_inst_t(instance_handle_t hInstance, instance_handle_t hPrevInstance, char* cmdLine, int cmdShow)
        : _instance(hInstance), _prevInstance(hPrevInstance), _cmdLine(cmdLine), _cmdShow(cmdShow)
    {}

    instance_handle_t instance() const { return _instance; }
    instance_handle_t prevInstance() const { return _prevInstance; }
    char* cmdLine() const { return _cmdLine; }
    int cmdShow() const { return _cmdShow; }
 
private:
    instance_handle_t _instance;
    instance_handle_t _prevInstance;
    char* _cmdLine;
    int _cmdShow;
};

using EntryContext = ptl::variant<args_t, wargs_t, args_inst_t>;

template <class CharT>
inline EntryContext MakeEntryContext(int argc, CharT* argv[])
{
    return base_args_t<CharT>(argc, argv);
}

inline EntryContext MakeEntryContext(instance_handle_t instance, instance_handle_t prevInstance, char* cmdLine, int cmdShow)
{
    return args_inst_t(instance, prevInstance, cmdLine, cmdShow);
}
}