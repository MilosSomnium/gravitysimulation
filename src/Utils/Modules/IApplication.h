#pragma once

class IApplication
{
public:
    virtual bool Init() = 0;
    virtual bool Run() = 0;
    virtual void Release() = 0;
};