#pragma once

#if defined(UNICODE) && defined(WIN32) 
#define MAIN_CONSOLE(ENTRY_FUNC)                          \
int wmain(int argc, wchar_t **argv)                       \
{                                                         \
    auto entry = aml::MakeEntryContext(argc, argv);         \
    return ENTRY_FUNC(std::move(entry));                   \
}
#else // Use standard main()
#define MAIN_CONSOLE(ENTRY_FUNC)                          \
int main(int argc, char **argv)                           \
{                                                         \
    auto entry = aml::MakeEntryContext(argc, argv);         \
    return ENTRY_FUNC(std::move(entry));                   \
}
#endif

#if defined(WIN32) 
#define MAIN(ENTRY_FUNC) \
int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int cmdShow) \
{                                                                                       \
    auto entry = aml::MakeEntryContext(hInstance, hPrevInstance, lpCmdLine, cmdShow);     \
    return ENTRY_FUNC(std::move(entry));                                                 \
}
#else
#define MAIN(ENTRY_FUNC) MAIN_CONSOLE(ENTRY_FUNC)
#endif