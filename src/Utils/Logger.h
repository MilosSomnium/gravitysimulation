#pragma once
#include <string>
#include <boost/format.hpp>

class Logger
{
public:
    enum class level_t : std::uint8_t
    {
        None = 0,
        Error,
        Warning,
        Info,
        Debug
    };

public:
    Logger(const Logger&) = delete;
    ~Logger() = default;

    Logger & operator=(const Logger&) = delete;

    inline static Logger& GetInstance()
    {
        static Logger m_instance;
        return m_instance;
    }

    void Init(const std::string& filename, bool useConsole = false);

    void SetConsoleOutput(bool use_stdout);
    void SetFileRolling(const std::string& filename);

    template <typename... Args>
    void operator()(const char* function_name, std::size_t line, level_t level, const std::string& fmt, const Args& ... args)
    {
        boost::format format(fmt);
        format.exceptions(boost::io::no_error_bits);
        Log(function_name, line, level, format, args...);
    }

    template<typename... Args>
    inline void Log(const char* function_name, std::size_t line, level_t level, const std::string& fmt, const Args&... args) {
        boost::format format(fmt);
        format.exceptions(boost::io::no_error_bits);
        Log(function_name, line, level, format, args...);
    }

    std::string GetLevelName(level_t level) const;
private:
    Logger() = default;

    void Log(const char* function_name, std::size_t line, level_t level, boost::format& format);

    template<typename Arg, typename... Args>
    inline void Log(const char* function_name, std::size_t line, level_t level, boost::format& format, Arg arg, Args... args) {
        format % Process(arg);
        Log(function_name, line, level, format, args...);
    }

    template<typename Type>
    inline Type Process(const Type& value)
    {
        return value;
    }

private:
    std::string m_logger;
};


#define LOGGER Logger::GetInstance()
#define LOG_INIT LOGGER.Init
#define LOG(level, str, ...) LOGGER(__FUNCTION__, __LINE__, level, str, ##__VA_ARGS__)
#if defined(_DEBUG)
#define LOG_DEBUG(str, ...) LOGGER(__FUNCTION__, __LINE__, Logger::level_t::Debug, str, ##__VA_ARGS__)
#else
#define LOG_DEBUG(str, ...)
#endif
#define LOG_INFO(str, ...) LOGGER(__FUNCTION__, __LINE__, Logger::level_t::Info, str, ##__VA_ARGS__)
#define LOG_WARNING(str, ...) LOGGER(__FUNCTION__, __LINE__, Logger::level_t::Warning, str, ##__VA_ARGS__)
#define LOG_ERROR(str, ...) LOGGER(__FUNCTION__, __LINE__, Logger::level_t::Error, str, ##__VA_ARGS__)

