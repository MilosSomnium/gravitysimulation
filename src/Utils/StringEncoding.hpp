#pragma once
#include <string>
#include "PTL/StringView.h"

namespace Utils {
namespace Encoding {

std::string to_utf8(std::uint32_t ch);
std::string to_utf8(ptl::wstring_view source);
std::wstring to_wstring(ptl::string_view source);
std::size_t utf8_length(ptl::string_view source);
}

}