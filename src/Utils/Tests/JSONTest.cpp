#include "UTUtilsDef.h"
#include "JSON/JSON.h"

using namespace JSON;

UT_FIXTURE_STRUCT(JSONTest)
{};



UT_FIXTURE_TEST_CASE(JSONTest, testNullJSONValue)
{
    JSON::Value value;
    UT_EXPECT_EQ(true, value.IsNull());
}

UT_FIXTURE_TEST_CASE(JSONTest, testBoolJSONValue)
{
    JSON::Value value(true);
    UT_EXPECT_EQ(true, value.IsBool());
    UT_EXPECT_EQ(true, value.ToBool());
}

UT_FIXTURE_TEST_CASE(JSONTest, testStringJSONValue)
{
    JSON::Value value("test str");
    UT_EXPECT_EQ(true, value.IsString());
    UT_EXPECT_EQ("test str", value.ToString());
}

UT_FIXTURE_TEST_CASE(JSONTest, testObjectJSONValue)
{
    JSON::Object object;
    object["bool"] = true;
    object["string"] = "string";
    const JSON::Value value(std::move(object));
    UT_EXPECT_EQ(true, value.IsObject());
    UT_EXPECT_EQ(true, value.ToObject()["bool"].ToBool());
    UT_EXPECT_EQ("string", value.ToObject()["string"].ToString());
    UT_EXPECT_EQ(true, value.ToObject()["null"].IsNull());
}

UT_FIXTURE_TEST_CASE(JSONTest, testArrayJSONValue)
{
    Object object;
    object["bool"] = true;
    object["string"] = "string";

    Array array;
    array.push_back(JSON::Value(object));
    array.push_back(JSON::Value(true));
    array.push_back(JSON::Value("string"));

    const JSON::Value value(std::move(array));
    UT_EXPECT_EQ(true, value.IsArray());
    UT_EXPECT_EQ(3, value.ToArray().size());
    {
        const auto& obj = value.ToArray()[0].ToObject();
        UT_EXPECT_EQ(true, obj["bool"].ToBool());
        UT_EXPECT_EQ("string", obj["string"].ToString());
        UT_EXPECT_EQ(true, obj["null"].IsNull());
    }

    UT_EXPECT_EQ(true, value.ToArray()[1].ToBool());
    UT_EXPECT_EQ("string", value.ToArray()[2].ToString());
}