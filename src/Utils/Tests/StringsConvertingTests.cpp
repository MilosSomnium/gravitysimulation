﻿#include "UTUtilsDef.h"
#include "StringEncoding.hpp" //#TODO: remove it

std::ostream& operator<< (std::ostream& os, const std::wstring& str)
{
    os << "[ ";
    for (const auto& ch : str)
    {
        os << "0x" << std::hex << ch << " ";
    }
    os << "]";
    return os;
}

using namespace Utils;

namespace {
const std::pair<std::string, std::wstring> strings[] = {
        {"ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                L"ABCDEFGHIJKLMNOPQRSTUVWXYZ" }, // Latin1
        {"\xCE\x90\xCE\x91\xCE\x92\xCE\x93\xCE\x94\xCE\x95\xCE\x96\xCE\x97\xCE\x98\xCE\x99\xCE\x9A\xCE\x9B\xCE\x9C\xCE\x9D\xCE\x9E\xCE\x9F",
                L"ΐΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟ" }, // Greek
        {"\xD0\x90\xD0\x91\xD0\x92\xD0\x93\xD0\x94\xD0\x95\xD0\x96\xD0\x97\xD0\x98\xD0\x99\xD0\x9A\xD0\x9B\xD0\x9C\xD0\x9D\xD0\x9E\xD0\x9F",
                L"АБВГДЕЖЗИЙКЛМНОП"} // Cyrillic
};
}


UT_TEST_CASE(StringEncodingTest)
{
    for (size_t index = 0; index < 3; ++index)
    {
        const auto value = strings[index];
        const auto& utf8String = value.first;
        const auto& wideString = value.second;

        UT_EXPECT_EQ(utf8String, Encoding::to_utf8(wideString));
        UT_EXPECT_EQ(wideString, Encoding::to_wstring(utf8String));
    }
}

UT_TEST_CASE(UTF8StringLengthTest)
{
    std::string test("Тестовая string");
    UT_EXPECT_EQ(15U, Encoding::utf8_length(test));
    UT_EXPECT_EQ(23U, test.size());
}