#pragma once
#include <boost/utility/string_ref.hpp>

namespace ptl
{
    template<class CharT, class Traits = std::char_traits<CharT>> 
    using basic_string_view = boost::basic_string_ref<CharT, Traits>;

    using string_view = basic_string_view<char>;
    using wstring_view = basic_string_view<wchar_t>;
}