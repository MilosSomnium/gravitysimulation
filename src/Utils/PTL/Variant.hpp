#pragma once
#include <boost/variant.hpp>

namespace ptl
{
    template <typename... T>
    using variant = boost::variant<T...>;

    template<typename Visitor, typename Variant>
    typename Visitor::result_type
    apply_visitor(Visitor & visitor, Variant & operand)
    {
        return boost::apply_visitor(visitor, operand);
    };
}