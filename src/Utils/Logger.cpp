#include "stdafx.h"
#include "Logger.h"
#include <iostream>
#include <thread>
#include <fmt/format.h>
#if defined(WIN32)
#include <windows.h>
#endif
#include "StringEncoding.hpp"


#define PATTERN_LAYOUT LOG4CPLUS_TEXT("%d{%d-%m-%y %H:%M:%S.%q} [%c{2}:%t] [%-5p] %m %n")

void Logger::Init(const std::string & filename, bool use_stdout/* = false*/)
{
    if (filename.empty())
    {
        // TODO:
        // get executable name
        // get executable file path
        // create directories if needs
        // set name for log

        SetFileRolling(filename);
    }
    else
    {
        SetFileRolling(filename);
    }
    
    SetConsoleOutput(use_stdout);
}

void Logger::SetConsoleOutput(bool use_stdout)
{
}

void Logger::SetFileRolling(const std::string & filename)
{
}

void Logger::Log(const char* function_name, std::size_t line, level_t level, boost::format& format)
{
    auto tid = 0;// std::this_thread::get_id();
    auto message = format.str();
    std::string formatedMessage;
    if (function_name && line > 0)
    {
        formatedMessage = fmt::format("{} [T:{}] {}: {} [{}:{}]", "", tid, GetLevelName(level), message, function_name , line);
    }
    else
    {
        formatedMessage = fmt::format("{} [T:{}] {}: {}", "", tid, GetLevelName(level), message);
    }

    std::cout << formatedMessage << std::endl;
#if defined(WIN32)
    ::OutputDebugStringW(Utils::Encoding::to_wstring(formatedMessage).c_str());
#endif
}

std::string Logger::GetLevelName(level_t level) const
{
    switch (level)
    {
    case level_t::None:
        return "";
        break;
    case level_t::Error:
        return "ERROR";
        break;
    case level_t::Warning:
        return "WARNING";
        break;
    case level_t::Info:
        return "INFO";
        break;
    case level_t::Debug:
        return "DEBUG";
    default:
        return "";
        break;
    }
    return "";
}

