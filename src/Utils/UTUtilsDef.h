#pragma once

#if defined(BOOST_UT_LIB)
#include <boost/test/unit_test.hpp>

#define UT_TEST_CASE(TestName) BOOST_AUTO_TEST_CASE(TestName)
#define UT_FIXTURE_TEST_CASE(Fixture, TestName) BOOST_FIXTURE_TEST_CASE(TestName, Fixture)
#define UT_FIXTURE_STRUCT(Fixture) struct Fixture
#define UT_FIXTURE_SETUP(Fixture) Fixture()
#define UT_FIXTURE_TEARDOWN(Fixture) ~Fixture()


#define UT_EXPECT_TRUE(Cond) BOOST_CHECK(Cond)
#define UT_EXPECT_EQ(L, R) BOOST_CHECK_EQUAL((L), (R))
#else

#include "3rdParty/microtest/microtest.h"
#define UT_TEST_CASE(TestName) TEST(TestName)
#define UT_FIXTURE_TEST_CASE(Fixture, TestName) TEST(TestName)
#define UT_FIXTURE_STRUCT(Fixture) struct Fixture
#define UT_FIXTURE_SETUP(Fixture) Fixture()
#define UT_FIXTURE_TEARDOWN(Fixture) ~Fixture()


#define UT_EXPECT_TRUE(Cond) ASSERT_TRUE(Cond)
#define UT_EXPECT_FALSE(Cond) ASSERT_FALSE(Cond)
#define UT_EXPECT_EQ(L, R) ASSERT_EQ((L), (R))
#define UT_EXPECT_NE(L, R) ASSERT_NEQ((L), (R))
#define UT_EXPECT_POINT_NEAR_EQ(L, R, AbsError) \
  if (!equalNear(L.x(), R.x(), AbsError) || !equalNear(L.y(), R.y(), AbsError)) {\
    printf("%s{    info} %s", mt::yellow(), mt::def());\
    std::cout << "Actual values: " << L << " != " << R << std::endl;\
    throw mt::AssertFailedException(#L " == " #R, __FILE__, __LINE__);\
  }\
  ASSERT(equalNear(L.x(), R.x(), AbsError) && equalNear(L.y(), R.y(), AbsError));
#endif
