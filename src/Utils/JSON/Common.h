 #pragma once
 #include <string>
 #include <cstdint>
  
 namespace JSON
 {
     enum class type_t : std::uint8_t
     {
         TypeNull = 0,
         TypeString,
         TypeNumber,
         TypeObject,
         TypeArray,
         TypeBool
     };
 
     using char_t = char;
     using string_t = std::basic_string<char_t>;
     using ostream_t = std::basic_ostream<char_t>;
     using istream_t = std::basic_istream<char_t>;
     using key_t = string_t;
 
     using int64_t = std::int64_t;
     using uint64_t = std::uint64_t;
     using int32_t = std::int32_t;
     using uint32_t = std::uint32_t;
     using int16_t = std::int16_t;
     using uint16_t = std::uint16_t;
     using int8_t = std::int8_t;
     using uint8_t = std::uint8_t;
     using real_t = double;
 }
