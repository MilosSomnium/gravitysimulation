#include "JSON.h"
#include <algorithm>
#include <ostream>
#include <iterator>

#include "Parser.h"

namespace
{
const std::string& NullLiteral()
{
    static const std::string literal("null");
    return literal;
}

const std::string& TrueLiteral()
{
    static const std::string literal("true");
    return literal;
}

const std::string& FalseLiteral()
{
    static const std::string literal("false");
    return literal;
}
}
using namespace JSON;
using namespace JSON::details;

Value::Value()
: m_value(std::make_unique<NullValue>())
{
    
}

Value::Value(const Value& that)
: m_value(that.m_value->Copy())
{

}

Value::Value(Value&& that) noexcept
: m_value(std::move(that.m_value))
{

}

Value::Value(const JSON::char_t* value)
: m_value(std::make_unique<StringValue>(value))
{

}

Value::Value(const JSON::string_t& value)
: m_value(std::make_unique<StringValue>(value))
{
}

Value::Value(const JSON::Number& value)
: m_value(std::make_unique<NumberValue>(value))
{
    
}

Value::Value(JSON::real_t value)
: m_value(std::make_unique<NumberValue>(value))
{

}

Value::Value(JSON::int64_t value)
: m_value(std::make_unique<NumberValue>(value))
{

}

Value::Value(JSON::uint64_t value)
: m_value(std::make_unique<NumberValue>(value))
{

}

Value::Value(const Object& value)
: m_value(std::make_unique<ObjectValue>(value))
{
}

Value::Value(const Array& value)
: m_value(std::make_unique<ArrayValue>(value))
{
    
}

Value::Value(bool value)
: m_value(std::make_unique<BoolValue>(value))
{
    
}

type_t Value::Type() const
{
    return m_value->Type();
}

bool Value::operator==(const Value& that) const
{
    return false;
}

Value& Value::operator=(Value& that)
{
    if (this != &that)
    {
        auto temp = that.m_value->Copy();
        m_value.swap(temp);
    }
    return *this;
}

Value& Value::operator=(Value&& that) noexcept
{
    if (this != &that)
    {
        auto temp = std::make_unique<NullValue>();
        std::swap(temp, that.m_value);
        m_value = std::move(temp);
    }
    return *this;
}

bool JSON::Value::IsNull() const
{
    return Type() == type_t::TypeNull;
}

bool JSON::Value::IsString() const
{
    return Type() == type_t::TypeString;
}

bool JSON::Value::IsNumber() const
{
    return Type() == type_t::TypeNumber;
}

bool JSON::Value::IsObject() const
{
    return Type() == type_t::TypeObject;
}

bool JSON::Value::IsArray() const
{
    return Type() == type_t::TypeArray;
}

bool JSON::Value::IsBool() const
{
    return Type() == type_t::TypeBool;
}

void JSON::Value::Serialize(ostream_t& stream) const
{
    m_value->Serialize(stream);
}

void JSON::Value::Deserialize(const string_t& stream)
{
    Parser parser;
    parser.Parse(stream, *this);
}

const string_t& Value::ToString() const
{
    return m_value->ToString();
}

const JSON::Number& Value::ToNumber() const
{
    return m_value->ToNumber();
}

const JSON::Object& Value::ToObject() const
{
    return m_value->ToObject();
}

const JSON::Array& Value::ToArray() const
{
    return m_value->ToArray();
}

bool Value::ToBool() const
{
    return m_value->ToBool();
}

JSON::uint64_t JSON::Value::ToUint64() const
{
    return m_value->ToNumber().ToUint64();
}

JSON::int64_t JSON::Value::ToInt64() const
{
    return m_value->ToNumber().ToInt64();
}

JSON::real_t JSON::Value::ToReal() const
{
    return m_value->ToNumber().ToReal();
}


Number::Number(JSON::real_t value)
: m_type(JSON::Number::type_t::real_type)
, m_realValue(value)
{

}

Number::Number(JSON::int8_t value)
: m_type(JSON::Number::type_t::signed_type)
, m_intValue(static_cast<JSON::int64_t>(value))
{

}

Number::Number(JSON::uint8_t value)
: m_type(JSON::Number::type_t::unsigned_type)
, m_uintValue(static_cast<JSON::uint64_t>(value))
{

}

Number::Number(JSON::int16_t value)
: m_type(JSON::Number::type_t::signed_type)
, m_intValue(static_cast<JSON::int64_t>(value))
{

}

Number::Number(JSON::uint16_t value)
: m_type(JSON::Number::type_t::unsigned_type)
, m_uintValue(static_cast<JSON::uint64_t>(value))
{

}

Number::Number(JSON::int32_t value)
: m_type(JSON::Number::type_t::signed_type)
, m_intValue(static_cast<JSON::int64_t>(value))
{

}

Number::Number(JSON::uint32_t value)
: m_type(JSON::Number::type_t::unsigned_type)
, m_uintValue(static_cast<JSON::uint64_t>(value))
{

}

Number::Number(JSON::int64_t value)
: m_type(JSON::Number::type_t::signed_type)
, m_intValue(value)
{

}

Number::Number(JSON::uint64_t value)
: m_type(JSON::Number::type_t::unsigned_type)
, m_uintValue(value)
{

}

Number::~Number()
{

}

bool Number::IsReal() const
{
    return m_type == Number::type_t::real_type;
}

bool Number::IsUint64() const
{
    bool result = 0;
    switch (m_type)
    {
    case JSON::Number::type_t::signed_type:
        result = m_intValue >= 0;
        break;
    case JSON::Number::type_t::unsigned_type:
        result = true;
        break;
    default:
        result = false;
        break;
    }
    return result;
}

JSON::real_t Number::ToReal() const
{
    JSON::real_t result = 0.0;
    switch (m_type)
    {
    case JSON::Number::type_t::real_type:
        result = m_realValue;
        break;
    case JSON::Number::type_t::signed_type:
        result = static_cast<real_t>(m_intValue);
        break;
    case JSON::Number::type_t::unsigned_type:
        result = static_cast<real_t>(m_uintValue);
        break;
    default:
        throw std::logic_error("Unknown number type");
    }
    return result;
}

JSON::uint64_t Number::ToUint64() const
{
    JSON::uint64_t result = 0;
    switch (m_type)
    {
    case JSON::Number::type_t::real_type:
        result = static_cast<uint64_t>(m_realValue);
        break;
    case JSON::Number::type_t::signed_type:
        result = static_cast<uint64_t>(m_intValue);
        break;
    case JSON::Number::type_t::unsigned_type:
        result = m_uintValue;
        break;
    default:
        throw std::logic_error("Unknown number type");
    }
    return result;
}

JSON::int64_t Number::ToInt64() const
{
    JSON::int64_t result = 0;
    switch (m_type)
    {
    case JSON::Number::type_t::real_type:
        result = static_cast<JSON::int64_t>(m_realValue);
        break;
    case JSON::Number::type_t::signed_type:
        result = m_intValue;
        break;
    case JSON::Number::type_t::unsigned_type:
        result = static_cast<JSON::int64_t>(m_uintValue);
        break;
    default:
        throw std::logic_error("Unknown number type");
    }
    return result;
}

JSON::uint32_t Number::ToUint32() const
{
    JSON::uint32_t result = 0;
    switch (m_type)
    {
    case JSON::Number::type_t::real_type:
        result = static_cast<JSON::uint32_t>(m_realValue);
        break;
    case JSON::Number::type_t::signed_type:
        result = static_cast<JSON::uint32_t>(m_intValue);
        break;
    case JSON::Number::type_t::unsigned_type:
        result = static_cast<JSON::uint32_t>(m_uintValue);
        break;
    default:
        throw std::logic_error("Unknown number type");
    }
    return result;
}

JSON::int32_t Number::ToInt32() const
{
    JSON::int32_t result = 0;
    switch (m_type)
    {
    case JSON::Number::type_t::real_type:
        result = static_cast<JSON::uint32_t>(m_realValue);
        break;
    case JSON::Number::type_t::signed_type:
        result = static_cast<JSON::uint32_t>(m_intValue);
        break;
    case JSON::Number::type_t::unsigned_type:
        result = static_cast<JSON::uint32_t>(m_uintValue);
        break;
    default:
        throw std::logic_error("Unknown number type");
    }
    return result;
}

void Number::Serialize(JSON::ostream_t& stream) const
{
    switch (m_type)
    {
    case JSON::Number::type_t::real_type:
        stream << std::to_string(m_realValue);
        break;
    case JSON::Number::type_t::signed_type:
        stream << std::to_string(m_intValue);
        break;
    case JSON::Number::type_t::unsigned_type:
        stream << std::to_string(m_uintValue);
        break;
    default:
        throw std::logic_error("Unknown number type");
    }
}

Object::Object()
{

}

Object::Object(const storage_type& values)
{
    m_values.clear();
    std::copy(values.begin(), values.end(), std::back_inserter<storage_type>(m_values));
}

Object::Object(const Object& that)
: Object(that.m_values)
{

}

Object::Object(Object&& that)
: m_values(std::move(that.m_values))
{

}

Object::~Object()
{

}

Object& Object::operator=(const Object& that)
{
    if (this != &that)
    {
        m_values.clear();
        std::copy(that.m_values.begin(), that.m_values.end(), std::back_inserter<storage_type>(m_values));
    }
    return *this;
}

Object& Object::operator=(Object&& that)
{
    if (this != &that)
    {
        m_values = std::move(that.m_values);
    }
    return *this;
}

Object::iterator Object::begin()
{
    return m_values.begin();
}

Object::const_iterator Object::begin() const
{
    return m_values.begin();
}

Object::iterator Object::end()
{
    return m_values.end();
}

Object::const_iterator Object::end() const
{
    return m_values.end();
}

Object::reverse_iterator Object::rbegin()
{
    return m_values.rbegin();
}

Object::const_reverse_iterator Object::rbegin() const
{
    return m_values.rbegin();
}

Object::reverse_iterator Object::rend()
{
    return m_values.rend();
}

Object::const_reverse_iterator Object::rend() const
{
    return m_values.rend();
}

std::pair<Object::iterator, bool> JSON::Object::insert(const value_type& value)
{
    std::pair<Object::iterator, bool> result;
    result.second = true;
    auto it = find_by_key(value.first);
    if (it != m_values.end())
    {
        it->second = value.second;
        result.second = false;
    }
    {
        it = m_values.insert(it, value);
    }
    result.first = it;
    return result;
}

Object::iterator Object::erase(iterator position)
{
    return m_values.erase(position);
}

void Object::erase(const key_t& key)
{
    auto it = find_by_key(key);
    m_values.erase(it);
}

Value& Object::at(const key_t& key)
{
    auto it = find_by_key(key);
    if (it == m_values.end())
    {
        throw std::out_of_range("Key not found");
    }
    return it->second;
}

const Value& Object::at(const key_t& key) const
{
    auto it = find_by_key(key);
    if (it == m_values.end())
    {
        throw std::out_of_range("Key not found");
    }
    return it->second;
}

Value& Object::operator[](const key_t& key)
{
    auto it = find_by_key(key);
    if (it == m_values.end())
    {
        it = m_values.insert(it, std::pair<key_t, Value>(key, Value()));
    }
    return it->second;
}

const Value& Object::operator[](const key_t& key) const
{
    auto it = find_by_key(key);
    if (it != m_values.end())
    {
        return it->second;
    }
    static const Value null;
    return null;
}

Object::size_type Object::size() const
{
    return m_values.size();
}

Object::iterator Object::find_by_key(const key_t& key)
{
    return std::find_if(m_values.begin(), m_values.end(), 
        [&key](const std::pair<key_t, Value>& p)
        {
            return p.first == key;
        });
}

Object::const_iterator Object::find_by_key(const key_t& key) const
{
    return std::find_if(m_values.begin(), m_values.end(), 
        [&key](const std::pair<key_t, Value>& p)
        {
            return p.first == key;
        });
}


void Object::Serialize(ostream_t& stream) const
{
    stream << "{";
    const auto& values = m_values;
    if(!values.empty())
    {
        auto lastElement = values.end() - 1;
        for (auto iter = values.begin(); iter != lastElement; ++iter)
        {
            const auto& key = iter->first;
            const auto& value = iter->second;
            SerializeKey(stream, key);
            stream << ':';
            value.Serialize(stream);
            stream << ',';
        }
        SerializeKey(stream, lastElement->first);
        stream << ':';
        lastElement->second.Serialize(stream);
    }
    stream << '}';
}

void Object::SerializeKey(ostream_t& stream, const key_t& key) const
{
    stream<< '\"' << key << '\"';
}

Array::Array()
{}

Array::Array(const storage_type& values)
{
    std::copy(values.begin(), values.end(), m_values.begin());
}

Array::Array(size_type size)
: m_values(size)
{}

JSON::Array::iterator Array::begin()
{
    return m_values.begin();
}

JSON::Array::iterator Array::end()
{
    return m_values.end();
}

JSON::Array::const_iterator Array::begin() const
{
    return m_values.begin();
}

JSON::Array::const_iterator Array::end() const
{
    return m_values.end();
}

JSON::Array::reverse_iterator Array::rbegin()
{
    return m_values.rbegin();
}

JSON::Array::reverse_iterator Array::rend()
{
    return m_values.rend();
}

JSON::Array::const_reverse_iterator Array::rbegin() const
{
    return m_values.rbegin();
}

JSON::Array::const_reverse_iterator Array::rend() const
{
    return m_values.rend();
}

JSON::Array::iterator Array::erase(iterator position)
{
    return m_values.erase(position);
}

void Array::erase(size_type index)
{
    if (index >= m_values.size())
    {
        throw std::out_of_range("index out of bounds");
    }
    m_values.erase(m_values.begin() + index);
}

Value& JSON::Array::at(size_type index)
{
    return m_values.at(index);
}

const Value& JSON::Array::at(size_type index) const
{
    return m_values.at(index);
}

Value& JSON::Array::operator[](size_type index)
{
    return m_values[index];
}

void JSON::Array::push_back(const Value& value)
{
    m_values.push_back(value);
}

bool JSON::Array::empty() const
{
    return m_values.empty();
}

void JSON::Array::Serialize(ostream_t& stream) const
{
    stream << '[';
    auto values = m_values;
    if(!values.empty())
    {
        auto lastElement = values.end() - 1;
        for (auto iter = values.begin(); iter != lastElement; ++iter)
        {
            iter->Serialize(stream);
            stream << ',';
        }
        lastElement->Serialize(stream);
    }
    stream << ']';
}

const Value& JSON::Array::operator[](size_type index) const
{
    return m_values[index];
}

JSON::Array::size_type Array::size() const
{
    return m_values.size();
}

// NullValue
NullValue::NullValue()
{
}

NullValue::~NullValue()
{
}

JSON::type_t NullValue::Type() const
{
    return type_t::TypeNull;
}

void NullValue::Serialize(ostream_t& stream) const
{
    stream << NullLiteral();
}

std::unique_ptr<NullValue> NullValue::Copy() const
{
    return std::make_unique<NullValue>();
}

const JSON::string_t& NullValue::ToString() const
{
    throw std::logic_error("Value is not string");
}

const JSON::Number& NullValue::ToNumber() const
{
    throw std::logic_error("Value is not number");
}

const JSON::Object& NullValue::ToObject() const
{
    throw std::logic_error("Value is not object");
}

const JSON::Array& NullValue::ToArray() const
{
    throw std::logic_error("Value is not array");
}

bool NullValue::ToBool() const
{
    throw std::logic_error("Value is not bool");
}

// StringValue
StringValue::StringValue(const string_t& value)
    : m_value(HandleUnescapedChars(value))
{}

StringValue::~StringValue()
{
}

JSON::type_t StringValue::Type() const
{
    return type_t::TypeString;
}

void StringValue::Serialize(ostream_t& stream) const
{
    stream << '\"' << m_value << '\"';
}

const JSON::string_t& StringValue::ToString() const
{
    return m_value;
}

std::unique_ptr<NullValue> StringValue::Copy() const
{
    return std::make_unique<StringValue>(*this);
}

string_t StringValue::HandleUnescapedChars(const string_t& value)
{
    string_t handledValue;
    for (auto iter = value.begin(); iter != value.end(); iter++)
    {
        auto ch = *iter;
        if (ch == '\\' || ch == '\"' || ch == '/' || ch == '\b' || ch == '\f' || ch == '\r' || ch == '\n' || ch == '\t')
        {
            //handledValue.push_back('\\');
            handledValue.push_back(ch);
        }
        else
        {
            handledValue.push_back(ch);
        }
    }

    return handledValue;
}

NumberValue::NumberValue(const Number& value)
    : m_value(value)
{

}

NumberValue::~NumberValue()
{

}

JSON::type_t NumberValue::Type() const
{
    return type_t::TypeNumber;
}

void NumberValue::Serialize(ostream_t& stream) const
{
    m_value.Serialize(stream);
}

const JSON::Number& NumberValue::ToNumber() const
{
    return m_value;
}

std::unique_ptr<NullValue> NumberValue::Copy() const
{
    return std::make_unique<NumberValue>(*this);
}

ObjectValue::ObjectValue(const Object& that)
    : m_value(that)
{

}

ObjectValue::~ObjectValue()
{

}

JSON::type_t ObjectValue::Type() const
{
    return type_t::TypeObject;
}

void ObjectValue::Serialize(ostream_t& stream) const
{
    m_value.Serialize(stream);
}

const JSON::Object& ObjectValue::ToObject() const
{
    return m_value;
}

std::unique_ptr<NullValue> ObjectValue::Copy() const
{
    return std::make_unique<ObjectValue>(*this);
}

ArrayValue::ArrayValue(const Array& value)
    : m_value(value)
{

}

ArrayValue::~ArrayValue()
{

}

JSON::type_t ArrayValue::Type() const
{
    return type_t::TypeArray;
}

void ArrayValue::Serialize(ostream_t& stream) const
{
    m_value.Serialize(stream);
}

const JSON::Array& ArrayValue::ToArray() const
{
    return m_value;
}

std::unique_ptr<NullValue> ArrayValue::Copy() const
{
    return std::make_unique<ArrayValue>(*this);
}

BoolValue::BoolValue(const bool& value)
    : m_value(value)
{

}

BoolValue::~BoolValue()
{

}

JSON::type_t BoolValue::Type() const
{
    return type_t::TypeBool;
}

void BoolValue::Serialize(ostream_t& stream) const
{
    stream << (m_value ? TrueLiteral() : FalseLiteral());
}

bool BoolValue::ToBool() const
{
    return m_value;
}

std::unique_ptr<NullValue> BoolValue::Copy() const
{
    return std::make_unique<BoolValue>(*this);
}
