#pragma once
#include <vector>
#include <memory>
#include "Common.h"

namespace JSON {

class Number;
class Object;
class Array;

namespace details {
class NullValue;
class ObjectValue;
class StringValue;
class NumberValue;
class ArrayValue;
}

using detail_value_t = std::unique_ptr<::JSON::details::NullValue>;

class Value
{
public:
    Value();
    Value(const Value& that);
    Value(Value&& that) noexcept;
    explicit Value(const char_t* value);
    explicit Value(const string_t& value);
    explicit Value(const Number& value);
    explicit Value(real_t value);
    explicit Value(uint64_t value);
    explicit Value(int64_t value);
    explicit Value(const Object& value);
    explicit Value(const Array& value);
    explicit Value(bool value);

    bool operator==(const Value& that) const;
    Value& operator=(Value&& that) noexcept;
    Value& operator=(Value& that);

    template <class T>
    Value& operator=(const T& that)
    {
        Value temp(that);
        std::swap(temp, *this);
        return *this;
    }

    type_t Type() const;

    bool IsNull() const;
    bool IsString() const;
    bool IsNumber() const;
    bool IsObject() const;
    bool IsArray() const;
    bool IsBool() const;

    friend std::ostream& operator<<(std::ostream& output, const Value& value)
    {
        value.Serialize(output);
        return output;
    }

    friend std::istream& operator>>(std::istream& input, Value& value)
    {
        return input;
    }

    void Serialize(ostream_t& stream) const;
    void Deserialize(const string_t& stream);

public:
    const string_t& ToString() const;
    const Number& ToNumber() const;
    const Object& ToObject() const;
    const Array& ToArray() const;
    bool ToBool() const;
    uint64_t ToUint64() const;
    int64_t ToInt64() const;
    real_t ToReal() const;

private:
    detail_value_t m_value;
};

class Object
{
public:
    using value_type = std::pair<key_t, Value>;
    using storage_type = std::vector<value_type>;
    using iterator = storage_type::iterator;
    using const_iterator = storage_type::const_iterator;
    using reverse_iterator = storage_type::reverse_iterator;
    using const_reverse_iterator = storage_type::const_reverse_iterator;
    using size_type = storage_type::size_type;

public:
    Object();
    Object(Object&& that);
    Object(const Object& that);
    Object(const storage_type& values);
    ~Object();

    Object& operator=(const Object& that);
    Object& operator=(Object&& that);

public:
    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;
    reverse_iterator rbegin();
    const_reverse_iterator rbegin() const;
    reverse_iterator rend();
    const_reverse_iterator rend() const;
    std::pair<iterator, bool> insert(const value_type& value);
    iterator erase(iterator position);
    void erase(const key_t& key);
    Value& at(const key_t& key);
    const Value& at(const key_t& key) const;
    Value& operator[](const key_t& key);
    const Value& operator[](const key_t& key) const;
    size_type size() const;

    void Serialize(ostream_t& stream) const;

private:
    iterator find_by_key(const key_t& key);
    const_iterator find_by_key(const key_t& key) const;
    void SerializeKey(ostream_t& stream, const key_t& key) const;

private:
    storage_type m_values;

    friend class Value;
};

class Number
{
private:
    enum class type_t : std::uint8_t
    {
        signed_type,
        unsigned_type,
        real_type
    };

public:
    Number(real_t value);
    Number(uint64_t value);
    Number(int64_t value);
    Number(uint32_t value);
    Number(int32_t value);
    Number(uint16_t value);
    Number(int16_t value);
    Number(uint8_t value);
    Number(int8_t value);
    ~Number();

    bool IsReal() const;
    bool IsUint64() const;

    real_t ToReal() const;
    uint64_t ToUint64() const;
    int64_t ToInt64() const;
    uint32_t ToUint32() const;
    int32_t ToInt32() const;

    void Serialize(ostream_t& stream) const;

private:
    union
    {
        real_t m_realValue;
        int64_t m_intValue;
        uint64_t m_uintValue;
    };

    type_t m_type;
};

class Array
{
    typedef std::vector<Value> storage_type;

public:
    typedef storage_type::iterator iterator;
    typedef storage_type::const_iterator const_iterator;
    typedef storage_type::reverse_iterator reverse_iterator;
    typedef storage_type::const_reverse_iterator const_reverse_iterator;
    typedef storage_type::size_type size_type;

public:
    Array();
    Array(size_type size);
    Array(const storage_type& values);

public:
    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;
    reverse_iterator rbegin();
    const_reverse_iterator rbegin() const;
    reverse_iterator rend();
    const_reverse_iterator rend() const;
    iterator erase(iterator position);
    void erase(size_type index);
    Value& at(size_type index);
    const Value& at(size_type index) const;
    Value& operator[](size_type index);
    const Value& operator[](size_type index) const;
    size_type size() const;
    void push_back(const Value& value);
    bool empty() const;

    void Serialize(ostream_t& stream) const;

private:
    storage_type m_values;

    friend class Value;
};

namespace details {
class NullValue
{
public:
    NullValue();
    virtual ~NullValue();

    virtual type_t Type() const;

    virtual void Serialize(ostream_t& stream) const;

    virtual const string_t& ToString() const;
    virtual const Number& ToNumber() const;
    virtual const Object& ToObject() const;
    virtual const Array& ToArray() const;
    virtual bool ToBool() const;

    virtual detail_value_t Copy() const;
};

class StringValue : public NullValue
{
public:
    StringValue(const string_t& value);
    virtual ~StringValue();

    virtual type_t Type() const override;

    virtual void Serialize(ostream_t& stream) const override;

    virtual const string_t& ToString() const override;

    virtual detail_value_t Copy() const override;

private:
    static string_t HandleUnescapedChars(const string_t& value);

private:
    string_t m_value;
};

class NumberValue : public NullValue
{
public:
    NumberValue(const Number& value);
    virtual ~NumberValue();

    virtual type_t Type() const override;

    virtual void Serialize(ostream_t& stream) const override;

    virtual const Number& ToNumber() const override;

    virtual detail_value_t Copy() const override;

private:
    Number m_value;
};

class ObjectValue : public NullValue
{
public:
    ObjectValue(const Object& that);
    virtual ~ObjectValue();

    virtual type_t Type() const override;

    virtual void Serialize(ostream_t& stream) const override;

    virtual const Object& ToObject() const override;

    virtual detail_value_t Copy() const override;

private:
    Object m_value;
};

class ArrayValue : public NullValue
{
public:
    ArrayValue(const Array& value);
    virtual ~ArrayValue();

    virtual type_t Type() const override;

    virtual void Serialize(ostream_t& stream) const override;

    virtual const Array& ToArray() const override;

    virtual detail_value_t Copy() const override;

private:
    Array m_value;
};

class BoolValue : public NullValue
{
public:
    BoolValue(const bool& value);
    virtual ~BoolValue();

    virtual type_t Type() const override;

    virtual void Serialize(ostream_t& stream) const override;

    virtual bool ToBool() const override;

    virtual detail_value_t Copy() const override;

private:
    bool m_value;
};
} // details

}
