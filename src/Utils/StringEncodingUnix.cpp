#include "StringEncoding.hpp"
#include <memory>
#include <boost/format.hpp>
#include "3rdParty/utf8/utf8.h"
#include "iconv.h"

namespace Utils {
namespace Encoding {

std::string to_utf8(std::uint32_t ch)
{
    std::string result;
    if (ch < 0x80) {        // one octet
        result.resize(1);
        result[0] = static_cast<char const>(ch);
    }
    else if (ch < 0x800) {   // two octets
        result.resize(2);
        result[0] = static_cast<char const>((ch >> 6) | 0xc0);
        result[1] = static_cast<char const>((ch & 0x3f) | 0x80);
    }
    else if (ch < 0x10000) { // three octets
        result.resize(3);
        result[0] = static_cast<char const>((ch >> 12) | 0xe0);
        result[1] = static_cast<char const>(((ch >> 6) & 0x3f) | 0x80);
        result[2] = static_cast<char const>((ch & 0x3f) | 0x80);
    }
    else {                   // four octets
        result[0] = static_cast<char const>((ch >> 18) | 0xf0);
        result[1] = static_cast<char const>(((ch >> 12) & 0x3f) | 0x80);
        result[2] = static_cast<char const>(((ch >> 6) & 0x3f) | 0x80);
        result[3] = static_cast<char const>((ch & 0x3f) | 0x80);
    }
    return result;
}

std::string to_utf8(ptl::wstring_view source)
{
    std::string result;
    utf8::utf16to8(source.begin(), source.end(), std::back_inserter(result));
    return result;
}

std::wstring to_wstring(ptl::string_view source)
{
    std::wstring result;
    utf8::utf8to16(source.begin(), source.end(), std::back_inserter(result));
    return result;
}

std::size_t utf8_length(ptl::string_view source)
{
    return utf8::distance(source.begin(), source.end());
}

}
}
