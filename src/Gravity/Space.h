#pragma once
#include <vector>
#include "Object.h"

namespace Gravity
{
class Space
{
public:
    void add(Object object);

    void draw(Graphics::Context& ctx);
    void move();
    void tick();
    void processGravity();

private:
    std::vector<Object> _objects;
};
}