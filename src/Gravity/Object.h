#pragma once
#include "Utils/Graphics/Point.h"
#include "Utils/Graphics/Color.h"

namespace Graphics
{
class Context;
}

namespace Gravity
{
class Object
{
public:
    using Unit = float;

public:
    Object(Unit mass, const Graphics::Point& pos, const Graphics::Point& velocity, const Graphics::Color& color);

    void draw(Graphics::Context& ctx);
    const Graphics::Point& getPosition() const;
    Unit getMass() const;
    Unit getRadius() const;

    const Graphics::Point& move();
    void applyForce(const Graphics::Point& force);

private:
    Unit _mass;
    Graphics::Point _position;
    Graphics::Point _lastPosition;
    Graphics::Point _velocity;
    Graphics::Color _color;
};
}