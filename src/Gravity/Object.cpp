#include "Object.h"
#include "Utils/Graphics/Context.h"

namespace Gravity
{
Object::Object(Unit mass, const Graphics::Point& pos, const Graphics::Point& velocity, const Graphics::Color& color)
: _mass(mass)
, _position(pos)
, _lastPosition(pos)
, _velocity(velocity)
, _color(color)
{}

void Object::draw(Graphics::Context& ctx)
{
    Graphics::ScopedStateSaver saver(ctx);
    ctx.setPenColor(_color);
    ctx.setBrushColor(_color);
    ctx.drawCircle(getPosition(), getRadius());
    ctx.drawLine(_lastPosition, getPosition());
}

const Graphics::Point& Object::getPosition() const
{
    return _position;
}

Object::Unit Object::getMass() const
{
    return _mass;
}

Object::Unit Object::getRadius() const
{
    return std::sqrt(_mass / 3.14159);
}

const Graphics::Point& Object::move()
{
    _lastPosition = _position;
    _position += _velocity;
    return _position;
}

void Object::applyForce(const Graphics::Point& force)
{
    _velocity += (force / _mass);
}
}