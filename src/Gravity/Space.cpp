#include "Space.h"
#include <numeric>

using namespace Graphics;

namespace Gravity
{
namespace
{
float getDistance(const Point& left, const Point& right)
{
    const auto xDist = left.x() - right.x();
    const auto yDist = left.y() - right.y();
    return std::sqrt(xDist*xDist + yDist*yDist);
}

float getGravityValue(Gravity::Object& left, Gravity::Object& right, bool& collision)
{
    const auto distance = getDistance(left.getPosition(), right.getPosition());
    if(distance < left.getRadius() + right.getRadius())
    {
        collision = true;
        return 0.0;
    }
    return (left.getMass() * right.getMass()) / (distance * distance);
}

}
void Space::add(Gravity::Object object)
{
    _objects.emplace_back(std::move(object));
}

void Space::move()
{
    for(auto& object : _objects)
    {
        object.move();
    }
}

void Space::draw(Context& ctx)
{
    for(auto& object : _objects)
    {
        object.draw(ctx);
    }
}

void Space::processGravity()
{
    const auto endIt = _objects.end();
    for(auto objectIt = _objects.begin(); objectIt != endIt; ++objectIt)
    {
        std::vector<Point> forces;
        forces.reserve(_objects.size());
        for(auto anotherObjectIt = _objects.begin(); anotherObjectIt != endIt; ++anotherObjectIt)
        {
            if(objectIt == anotherObjectIt)
            {
                continue;
            }
            
            bool collision = false;
            const auto gravityValue = getGravityValue(*objectIt, *anotherObjectIt, collision);
            if(!collision)
            {
                forces.emplace_back(normalize(anotherObjectIt->getPosition() - objectIt->getPosition()) * gravityValue);
            }
            else
            {
                // TODO: implement me
            }
        }

        objectIt->applyForce(std::accumulate(forces.begin(), forces.end(), Point()));
    }
}

void Space::tick()
{
    processGravity();
    move();
}

}