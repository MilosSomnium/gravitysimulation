#include "UTUtilsDef.h"
#include "Gravity/Space.h"
#include "Gravity/Object.h"

namespace
{
#include <cmath>
bool equalNear(double left, double right, double absError)
{
    return std::fabs(left - right) < absError;
}
}
using namespace Gravity;
using namespace Graphics;

UT_FIXTURE_STRUCT(ObjectTest)
{};

UT_FIXTURE_TEST_CASE(ObjectTest, testSingleObjectMoving)
{
    Gravity::Object object(1.0, {0.0, 0.0}, {5.0, 0.0}, Color(0, 0, 0));

    object.move();
    //
    UT_EXPECT_EQ(Point(5.0, 0.0), object.getPosition());

    object.move();
    //
    UT_EXPECT_EQ(Point(10.0, 0.0), object.getPosition());
}

UT_FIXTURE_TEST_CASE(ObjectTest, testSingleObjectMovingWithSlowing)
{
    Gravity::Object object(1.0, {0.0, 0.0}, {5.0, 0.0}, Color(0, 0, 0));

    std::vector<Point> expectingValues{{4.0, 0.0}, {7.0, 0.0}, {9.0, 0.0}, {10.0, 0.0}, {10.0, 0.0}};
    std::vector<Point> result;
    for(int count = 5; count > 0; --count)
    {
        object.applyForce({-1.0, 0.0});
        result.emplace_back(object.move());
    }

    for(size_t i = 0; i < expectingValues.size(); ++i)
    {
        UT_EXPECT_POINT_NEAR_EQ(expectingValues[i], result[i], 0.001);
    }   
}

UT_FIXTURE_TEST_CASE(ObjectTest, testSingleObjectMovingByCircle)
{
    Gravity::Object object(1.0, {5.0, 10.0}, {1.0, 0.0}, Color(0, 0, 0));

    const Point center(5.0, 5.0);
    auto getCentralForce = [&center](const Point& point) -> Point
    {
        return normalize(center - point);
    };
    
    std::vector<Point> expectingValues{{6.0, 9.0},
                                       {6.75746, 7.02986},
                                       {6.86036, 4.30370},
                                       {6.02672, 1.92808},
                                       {4.87608, 0.50088},
                                       {3.75298, 0.0733133},
                                       {2.87525, 0.615167},
                                       {2.43359, 2.05693},
                                       {2.64916, 4.25239},
                                       {3.8177, 6.75091}};
    std::vector<Point> result;
    for(int i = 0; i < expectingValues.size(); ++i)
    {
        object.applyForce(getCentralForce(object.getPosition()));
        result.emplace_back(object.move());
    }

    for(size_t i = 0; i < expectingValues.size(); ++i)
    {
        UT_EXPECT_POINT_NEAR_EQ(expectingValues[i], result[i], 0.001);
    }
}

UT_FIXTURE_STRUCT(GravityTest)
{};


